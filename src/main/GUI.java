package main;


import acciones.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

/**
 * Clase que contiene la ventana principial, botones y funciones del juego.
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class GUI extends JPanel{
    //Botones y etiquetas
    static public JButton reset, pedir, rendir, quedarse, asegurar, separar, doblar, jugar, nuevoJ;
    static public JButton[][][] cartJug = new JButton[7][5][2]; //7 Jugadores x 5 Cartas x 2 juegos
    static public JButton[] deal = new JButton[5]; //1 dealer x 7 cartas
    static public JLabel[][] info = new JLabel[7][2]; //7 Jugadores x 2 juegos
    static public JLabel datos, dealInfo;
    static public JLabel[][] turno = new JLabel[7][2];
    //Jugadores
    static public Jugador[] jugadores = new Jugador[7];
    static public Jugador dealer = null;
    //Otras variables
    static public Cartas Baraja = null; //Baraja que se va a usar
    static public int numJugadores = 0; //Numero de jugadores inicial(1)
    static public int entrada = 0; //Costo de la entrada
    static public JFrame frame; //Marco principal
    static public final int DEALER = 7; //Numero reservado del dealer
    
    //Constructor: Crea el marco y todos los botones
    public GUI(){
        //Creacion de la ventana
        frame = new JFrame("Blackjack");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        /* Boton para reiniciar */
        reset = new JButton("Iniciar");
        reset.setBounds(0, 0, 90, 20);
        reset.setActionCommand("iniciar");
        reset.setMnemonic(KeyEvent.VK_I);
        reset.setToolTipText("Alt-I");
        reset.setDisplayedMnemonicIndex(0);
        reset.addActionListener(new Inicializar());
        frame.add(reset);
        /* Tomar carta */
        pedir = new JButton("Pedir carta");
        pedir.setBounds(90,0,110,20);
        pedir.setEnabled(false);
        pedir.setMnemonic(KeyEvent.VK_P);
        pedir.setToolTipText("Alt-p");
        pedir.setDisplayedMnemonicIndex(0);
        pedir.setActionCommand("0");
        pedir.addActionListener(new darCarta());
        frame.add(pedir);
        /* Quedarse */
        quedarse = new JButton("Quedarse");
        quedarse.setBounds(200,0,90,20);
        quedarse.setEnabled(false);
        quedarse.setMnemonic(KeyEvent.VK_Q);
        quedarse.setToolTipText("Alt-q");
        quedarse.setDisplayedMnemonicIndex(0);
        quedarse.setActionCommand("0");
        quedarse.addActionListener(new Quedarse());
        frame.add(quedarse);
        /* Rendirse */
        rendir = new JButton("Rendirse");
        rendir.setBounds(0,20,75,20);
        rendir.setMargin(new Insets(0,0,0,0));
        rendir.setMnemonic(KeyEvent.VK_R);
        rendir.setToolTipText("Alt-r");
        rendir.setDisplayedMnemonicIndex(0);
        rendir.setEnabled(false);
        rendir.setActionCommand("0");
        rendir.addActionListener(new Rendirse());
        frame.add(rendir);
        /* Doblar */
        doblar = new JButton("Doblar");
        doblar.setBounds(75,20,60,20);
        doblar.setMargin(new Insets(0,0,0,0));
        doblar.setMnemonic(KeyEvent.VK_D);
        doblar.setToolTipText("Alt-d");
        doblar.setDisplayedMnemonicIndex(0);
        doblar.setEnabled(false);
        doblar.setActionCommand("0");
        doblar.addActionListener(new Doblar());
        frame.add(doblar);
        /* Separar */
        separar = new JButton("Separar");
        separar.setBounds(135,20,80,20);
        separar.setMnemonic(KeyEvent.VK_S);
        separar.setToolTipText("Alt-s");
        separar.setDisplayedMnemonicIndex(0);
        separar.setActionCommand("0");
        separar.addActionListener(new Separar());
        separar.setEnabled(false);
        frame.add(separar);
        /* Asegurar */
        asegurar = new JButton("Asegurar");
        asegurar.setBounds(215,20,70,20);
        asegurar.setMargin(new Insets(0,0,0,0));
        asegurar.setMnemonic(KeyEvent.VK_A);
        asegurar.setToolTipText("Alt-a");
        asegurar.setDisplayedMnemonicIndex(0);
        asegurar.setEnabled(false);
        asegurar.setActionCommand("0");
        asegurar.addActionListener(new Asegurar());
        frame.add(asegurar);
        
        //Cartas de cada jugador con su etiqueta al lado e indicador de turno
        for(int s = 0;s < 2;s++){
            for(int j = 0;j<7;j++){
                for(int i = 0;i<5;i++){
                    cartJug[j][i][s] = new JButton();
                    cartJug[j][i][s].setBounds(15+(30*i)+(285*s),55+(40*(j)),30,40);
                    cartJug[j][i][s].setMargin(new Insets(0,0,0,0));
                    cartJug[j][i][s].setEnabled(false);
                    if(s > 0)
                        cartJug[j][i][s].setVisible(true);
                    frame.add(cartJug[j][i][s]);
                }
                turno[j][s] = new JLabel();
                turno[j][s].setBounds(285*s,60+(40*j),15,30);
                frame.add(turno[j][s]);
                info[j][s] = new JLabel();
                info[j][s].setBounds(165+(285*s),55+(j*40),200,40);
                frame.add(info[j][s]);
            }
        }
        //Cartas de cada jugador al separar con su etiqueta al lado e indicador de turno
        /*for(int j = 0;j<7;j++){
            for(int i = 0;i<5;i++){
                cartSep[j][i] = new JButton();
                cartSep[j][i].setBounds(300+(30*i),55+(40*(j)),30,40);
                cartSep[j][i].setMargin(new Insets(0,0,0,0));
                cartSep[j][i].setVisible(false);
                cartSep[j][i].setEnabled(false);
                frame.add(cartSep[j][i]);
            }
            turnoSep[j] = new JLabel();
            turnoSep[j].setBounds(285,60+(40*j),15,30);
            frame.add(turnoSep[j]);
            info[j][1] = new JLabel();
            info[j][1].setBounds(450,55+(j*40),200,40);
            frame.add(info[j][1]);
        }*/
        //Cartas del dealer
        JLabel dealLab = new JLabel("Dealer");
        dealLab.setBounds(0, 335, 40, 15);
        frame.add(dealLab);
        for(int i = 0;i<5;i++){
            deal[i] = new JButton();
            frame.add(deal[i]);
            deal[i].setBounds(30*(i),355,30,40);
            deal[i].setMargin(new Insets(0,0,0,0));
            deal[i].setEnabled(false);
        }
        dealInfo = new JLabel();
        dealInfo.setBounds(150,355,200,30);
        frame.add(dealInfo);
        
        /* Comenzar el juego */
        jugar = new JButton("Comenzar juego");
        jugar.setBounds(20,395,120,20);
        jugar.setMargin(new Insets(0,0,0,0));
        jugar.setEnabled(false);
        jugar.setActionCommand("new");
        jugar.setMnemonic(KeyEvent.VK_N);
        jugar.setToolTipText("Alt-n");
        jugar.setDisplayedMnemonicIndex(4);
        jugar.addActionListener(new empezarJuego());
        frame.add(jugar);
        /* Nuevo jugador */
        nuevoJ = new JButton("Nuevo jugador");
        nuevoJ.setBounds(140,395,120,20);
        nuevoJ.setMargin(new Insets(0,0,0,0));
        nuevoJ.setEnabled(false);
        nuevoJ.addActionListener(new nuevoJugador());
        nuevoJ.setMnemonic(KeyEvent.VK_J);
        nuevoJ.setToolTipText("Alt-j");
        nuevoJ.setDisplayedMnemonicIndex(6);
        frame.add(nuevoJ);
        /* Informacion de jugadores (nombre y saldo)*/
        datos = new JLabel();
        datos.setBounds(0,415,300,120);
        datos.setVerticalAlignment(JLabel.TOP);
        frame.add(datos);

        //Solucion a un error en el que el ultimo elemento no aparecia
        JLabel algo = new JLabel();
        frame.add(algo);
        
        //Dimensiones y posicion de la ventana
        frame.pack();
        frame.setSize(new Dimension(300, 600));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize(); //Medidas de la pantalla
        frame.setLocation((dim.width-frame.getSize().width)/2, (dim.height-frame.getSize().height)/2); //Centrar
        frame.setResizable(false); //Bloquear cambiar tamaño de ventana
        frame.setVisible(true);
        
    }
    
    /* Actualiza la informacion mostrada al lado de las cartas de cada jugador */
    static public void updateLabel(int jugador){
        String str = "</html>";
        if(jugadores[jugador].asegurar){
            str = "<br>Seguro: $"+jugadores[jugador].apuesta[0]/2+"</html>";
        }
        info[jugador][0].setText("<html>$"+jugadores[jugador].apuesta[0]+"  Valor: "+jugadores[jugador].getValor()+str);
    }
    /* Actualiza la informacion mostrada al lado de las cartas de cada jugador (juego separado)*/
    static public void updateLabelSep(int jugador){
        info[jugador][1].setText("<html>$"+jugadores[jugador].apuesta[1]+"  Valor: "+jugadores[jugador].getValor(true)+"</html>");
    }
    
    /* Actualiza la informacion mostrada en la parte inferior, revisando todas las condiciones actuales */
    static public void updateDatos(){
        String str = "<html>";
        for(int i = 0;i<=numJugadores;i++){
            str += jugadores[i].nombre + " : $" + jugadores[i].dinero + "<br>";
        }
        str += "</html>";
        datos.setText(str);
    }
    
    /* Muestra en la mesa la carta */
    static public void mostrarCarta(int jugador, int numCarta, String carta, boolean oculta){
        if(jugador == DEALER){
            if(oculta){
                deal[numCarta].setText("");
                deal[numCarta].setForeground(Color.red);
                
                deal[numCarta].setBackground(Color.red);
            }else{
                deal[numCarta].setText(carta);
                deal[numCarta].setBackground(Color.white);
                if(carta.charAt(1) == '♥' || carta.charAt(1) == '♦')
                    deal[numCarta].setForeground(Color.red);
                else
                    deal[numCarta].setForeground(Color.black);
                    
            }
            deal[numCarta].setEnabled(true);
        }else{
            cartJug[jugador][numCarta][0].setText(carta);
            cartJug[jugador][numCarta][0].setBackground(Color.white);
            if(carta.charAt(1) == '♥' || carta.charAt(1) == '♦')
                cartJug[jugador][numCarta][0].setForeground(Color.red);
            else
                cartJug[jugador][numCarta][0].setForeground(Color.black);
            cartJug[jugador][numCarta][0].setEnabled(true);
        }
    }
    /* Quita la carta del tablero */
    static public void borrarCarta(int jugador, int numCarta){
        cartJug[jugador][numCarta][0].setText("");
        cartJug[jugador][numCarta][0].setBackground(null);
        cartJug[jugador][numCarta][0].setEnabled(false);
    }
    
    /* Actualiza la informacion mostrada al lado de las cartas de cada jugador */
    static public void mostrarCartaSeparar(int jugador, int numCarta, String carta){
        cartJug[jugador][numCarta][1].setText(carta);
        cartJug[jugador][numCarta][1].setBackground(Color.white);
        if(carta.charAt(1) == '♥' || carta.charAt(1) == '♦')
            cartJug[jugador][numCarta][1].setForeground(Color.red);
        else
            cartJug[jugador][numCarta][1].setForeground(Color.black);
        cartJug[jugador][numCarta][1].setEnabled(true);
    }
    
    /* Regresa el tablero a sus posiciones iniciales */
    static public void resetTablero(){
        //Borrar cartas dealer
        for(int i = 0;i<5;i++){
            deal[i].setText("");
            deal[i].setBackground(null);
            deal[i].setEnabled(false);  
        }
        //Borrar cartas jugador
        for(int j = 0;j<7;j++){
            for(int i = 0;i<5;i++){
                cartJug[j][i][0].setText("");
                cartJug[j][i][0].setBackground(null);
                cartJug[j][i][0].setEnabled(false);
            }
        }
        //Borrar y ocultar cartas separadas del jugador
        for(int j = 0;j<7;j++){
            for(int i = 0;i<5;i++){
                cartJug[j][i][1].setText("");
                cartJug[j][i][1].setBackground(null);
                cartJug[j][i][1].setEnabled(false);
                cartJug[j][i][1].setVisible(false);
            }
        }
        //Regresar la ventana a su estado inicial
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize(); //Medidas de la pantalla
        frame.setLocation((dim.width-frame.getSize().width)/2, (dim.height-frame.getSize().height)/2); //Centrar
        frame.setResizable(false); //Bloquear cambiar tamaño de ventana
    }
    
    //Mostrar si se gano o perdio la partida
    static public void resultado(int j, boolean gano, double mult){
        String str;
        int cantidad = (int)(jugadores[j].apuesta[0]*mult);
        if(gano){
            if(mult <= 1){
                str = "Recuperaste $"+cantidad; 
            }else{
                str = "Ganaste $"+cantidad;
            }
            jugadores[j].dinero += cantidad;
        }else{
            str = "Perdiste $"+cantidad;
        }
        JOptionPane.showMessageDialog(null, str, jugadores[j].nombre, JOptionPane.INFORMATION_MESSAGE);
        jugadores[j].apuesta[0] = 0;
        jugadores[j].numCartas[0] = 0;
        jugadores[j].rendir = false;
        updateLabel(j);
        updateDatos();
    }
    //Mostrar si se gano o perdio la partida
    static public void resultadoSep(int j, boolean gano, double mult){
        String str;
        int cantidad = (int)(jugadores[j].apuesta[1]*mult);
        if(gano){
            if(mult <= 1){
                str = "Recuperaste $"+cantidad;
            }else{
                str = "Ganaste $"+cantidad;
            }
            jugadores[j].dinero += cantidad;
        }else{
            str = "Perdiste $"+cantidad;
        }
        JOptionPane.showMessageDialog(null, str, jugadores[j].nombre, JOptionPane.INFORMATION_MESSAGE);
        jugadores[j].apuesta[1] = 0;
        jugadores[j].numCartas[1] = 0;
        jugadores[j].rendir = false;
        updateLabel(j);
        updateDatos();
    }
    /* Coloca un indicador en el jugador en turno */
    static public void setTurno(int j){
        if(j < 7){
            for(int i = 0;i <= numJugadores; i++){
                if(i == j){
                    turno[i][0].setText("►");
                }else{
                    turno[i][0].setText("");
                }
                turno[i][1].setText("");
            }
        }else{
            j-=7;
            for(int i = 0;i <= numJugadores; i++){
                if(i == j){
                    turno[i][1].setText("►");
                }else{
                    turno[i][1].setText("");
                }
                turno[i][0].setText("");
            }   
        }
    }
    
    /* Revisa el estado del juego de todos los jugadores */
    static public void checarJuego(){
        for(int i = 0; i <= numJugadores; i++){
            setTurno(i);
            if(jugadores[i].asegurar){
                if((dealer.getValor() == 21)&&(dealer.numCartas[0] == 1)){
                    JOptionPane.showMessageDialog(null, "Ganaste seguro de $"+ jugadores[i].apuesta[0], jugadores[i].nombre, JOptionPane.INFORMATION_MESSAGE);
                    jugadores[i].dinero += jugadores[i].apuesta[0];
                }else{
                    JOptionPane.showMessageDialog(null, "Perdiste seguro de $"+ jugadores[i].apuesta[0]/2, jugadores[i].nombre, JOptionPane.INFORMATION_MESSAGE);
                }
            }
            jugadores[i].asegurar = false;
            updateLabel(i);
            //Rendirse
            if(jugadores[i].rendir){
                resultado(i, true, 0.5);
            //Blackjack dealer y jugador
            }else if(((dealer.getValor() == 21)&&(dealer.numCartas[0] == 2))
                    &&((jugadores[i].getValor() == 21)&&(jugadores[i].numCartas[0] == 2))){
                resultado(i, true, 1);
            //Blackjack Dealer
            }else if((dealer.getValor() == 21)&&(dealer.numCartas[0] == 2)){
                resultado(i, false, 1);
            //Blackjack jugador
            }else if((jugadores[i].getValor() == 21)&&(jugadores[i].numCartas[0] == 2)){
                resultado(i, true, 3);
            //5 cartas sin pasar de 21
            }else if((jugadores[i].numCartas[0] == 5)&&(jugadores[i].getValor() < 21)){
                resultado(i, true, 2);
            //Pasarse de 21
            }else if(jugadores[i].getValor() > 21){
                resultado(i, false, 1);
            //Mas que el dealer, o el dealer se paso
            }else if(jugadores[i].getValor() > dealer.getValor() || dealer.getValor() > 21){
                resultado(i, true, 2);
            //Menos que el dealer
            }else if(jugadores[i].getValor() < dealer.getValor()){
                resultado(i, false, 1);
            //Igual que el dealer
            }else if(jugadores[i].getValor() == dealer.getValor()){
                resultado(i,true,1);
            }
            updateLabel(i);
            if(jugadores[i].separar){
                setTurno(i+7);
                //Blackjack dealer y jugador
                if(((dealer.getValor() == 21)&&(dealer.numCartas[1] == 2))
                        &&((jugadores[i].getValor(true) == 21)&&(jugadores[i].numCartas[1] == 2))){
                    resultadoSep(i, true, 1);
                //Blackjack Dealer
                }else if((dealer.getValor() == 21)&&(dealer.numCartas[1] == 2)){
                    resultadoSep(i, false, 1);
                //Blackjack jugador
                }else if((jugadores[i].getValor(true) == 21)&&(jugadores[i].numCartas[1] == 2)){
                    resultadoSep(i, true, 3);
                //5 cartas sin pasar de 21
                }else if((jugadores[i].numCartas[1] == 5)&&(jugadores[i].getValor(true) < 21)){
                    resultadoSep(i, true, 2);
                //Pasarse de 21
                }else if(jugadores[i].getValor(true) > 21){
                    resultadoSep(i, false, 1);
                //Mas que el dealer, o el dealer se paso
                }else if(jugadores[i].getValor(true) > dealer.getValor() || dealer.getValor() > 21){
                    resultadoSep(i, true, 2);
                //Menos que el dealer
                }else if(jugadores[i].getValor() < dealer.getValor()){
                    resultadoSep(i, false, 1);
                //Igual que el dealer
                }else if(jugadores[i].getValor(true) == dealer.getValor()){
                    resultadoSep(i,true,1);
                }
                updateLabelSep(i);
            }
            
        }
        
        setTurno(-1);
        jugar.setText("Nueva ronda");
        jugar.setEnabled(true);
        pedir.setEnabled(false);
        quedarse.setEnabled(false);
        rendir.setEnabled(false);
        separar.setEnabled(false);
        doblar.setEnabled(false);
        asegurar.setEnabled(false);
        jugar.setActionCommand("ronda");
    }
    
    //La jugada que realiza el dealer
    static public void jugarDealer(){
        mostrarCarta(DEALER, 0, dealer.cartas[0][0], false);
        while((dealer.getValor() <= 16) && (dealer.numCartas[0] < 5)){
                dealer.agregarCarta(Baraja.tomarCarta());
                mostrarCarta(DEALER, dealer.numCartas[0]-1, dealer.getLastCarta(), false);           
        }
        dealInfo.setText("Valor: "+dealer.getValor());
    }
    
    /* Prepara los botones para el jugador que sigue */
    static public void pasarTurno(int turno){
        if(turno < 7){
            rendir.setEnabled(true);
            rendir.setActionCommand(""+turno);
            if(dealer.getLastCarta().charAt(0) == 'A')
                asegurar.setEnabled(true);
            else
                asegurar.setEnabled(false);
            asegurar.setActionCommand(""+turno);
            if(jugadores[turno].cartas[0][0].charAt(0) == jugadores[turno].cartas[1][0].charAt(0))
                separar.setEnabled(true);
            else
                separar.setEnabled(false);
            separar.setActionCommand(""+turno);
            setTurno(turno);
        }else
            setTurno(turno);
        doblar.setEnabled(true);
        doblar.setActionCommand(""+turno);
        pedir.setEnabled(true);
        pedir.setActionCommand(""+turno);
        quedarse.setEnabled(true);
        quedarse.setActionCommand(""+turno);
        
    }
}