package main;

import java.util.*;

/**
 * Clase que maneja todo lo relacionado con cartas
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class Cartas {
    //Cartas en el juego(2 barajas)
    public String[] baraja = {
        "A♥", "2♥", "3♥", "4♥", "5♥", "6♥", "7♥", "8♥", "9♥", "J♥", "Q♥", "K♥",
        "A♦", "2♦", "3♦", "4♦", "5♦", "6♦", "7♦", "8♦", "9♦", "J♦", "Q♦", "K♦",
        "A♣", "2♣", "3♣", "4♣", "5♣", "6♣", "7♣", "8♣", "9♣", "J♣", "Q♣", "K♣",
        "A♠", "2♠", "3♠", "4♠", "5♠", "6♠", "7♠", "8♠", "9♠", "J♠", "Q♠", "K♠",
        "A♥", "2♥", "3♥", "4♥", "5♥", "6♥", "7♥", "8♥", "9♥", "J♥", "Q♥", "K♥",
        "A♦", "2♦", "3♦", "4♦", "5♦", "6♦", "7♦", "8♦", "9♦", "J♦", "Q♦", "K♦",
        "A♣", "2♣", "3♣", "4♣", "5♣", "6♣", "7♣", "8♣", "9♣", "J♣", "Q♣", "K♣",
        "A♠", "2♠", "3♠", "4♠", "5♠", "6♠", "7♠", "8♠", "9♠", "J♠", "Q♠", "K♠"
    };
    int n;
    //Constructor: Barajea las cartas y reinicia el contador
    public Cartas(){
        barajear();
        this.n = 0;
    }
    
    //Revuelve el arreglo de cartas
    public void barajear(){
        Collections.shuffle(Arrays.asList(this.baraja));
    }
    
    //Toma una carta y avanza el contador de cartas tomadas, regresa el texto de la carta
    public String tomarCarta(){
        String str = this.baraja[this.n];
        this.n++;
        return str;
    }
    
}
/*
class test{
    public static void main(String[] args){
        Cartas baraja = new Cartas();
        for(int i = 0; i < baraja.baraja.length; i++){
            System.out.println(baraja.baraja[i]);
        }
    }
}*/