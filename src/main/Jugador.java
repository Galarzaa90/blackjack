package main;

import javax.swing.*;

/**
 * Clae que maneja todo lo relacionado con los jugadores
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class Jugador {
    public String nombre;
    public int dinero = 0;
    public int[] apuesta = new int[2];
    public int[] numCartas = new int[2];
    public boolean rendir = false, asegurar = false, separar = false;
    public String[][] cartas = new String[5][2];
    //Constructor: Pregunta nombre y apuesta
    public Jugador(){
        boolean repetir;
        this.dinero = GUI.entrada;
        //Preguntar nombre
        do{
            repetir = false;
            this.nombre = JOptionPane.showInputDialog(GUI.frame,"Introduce tu nombre:","Jugador");
            //Revisar que no este vacio
            if(this.nombre.equals("")){
                JOptionPane.showMessageDialog(GUI.frame, "Debes introducir tu nombre", "Error", JOptionPane.WARNING_MESSAGE);
                continue;
            }
            for(int i = 0;i < this.nombre.length();i++){
                //Revisar que no se introduzca ningun numero
                if(Character.isDigit(this.nombre.charAt(i))){
                    JOptionPane.showMessageDialog(GUI.frame, "Tu nombre no puede tener numeros", "Error", JOptionPane.WARNING_MESSAGE);
                    repetir = true;
                    break;
                }
            }
        }while(this.nombre.equals("")||repetir);
        //Preguntar apuesta inicial
        do{
            String str = JOptionPane.showInputDialog("Apuesta (Minimo $10):","10");
            try{
                this.apuesta[0] = Integer.parseInt(str);
            } catch(NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Debe ser un valor numerico", "Error", JOptionPane.WARNING_MESSAGE);
                continue;
            }
            if(this.dinero<this.apuesta[0])
                JOptionPane.showMessageDialog(null, "No puedes apostar mas de lo que tienes!", "Error", JOptionPane.WARNING_MESSAGE);
            if(this.apuesta[0] < 10)
                JOptionPane.showMessageDialog(null, "La apuesta minima es $10.", "Error", JOptionPane.WARNING_MESSAGE);
        }while(this.dinero<this.apuesta[0] || this.apuesta[0] < 10);
        this.dinero -= this.apuesta[0];
    }
    //Constructor: Dealer
    public Jugador(boolean b){
        
    }
    //Metodo para agregar carta al jugador
    public void agregarCarta(String carta, boolean separar){
        int s;
        if(separar) s = 1;
        else s = 0;
        
        this.cartas[this.numCartas[s]][s] = carta;
        this.numCartas[s]++;
    }
    //En caso de no poner el segundo parametro, se llama al metodo anterior con separar = false
    public void agregarCarta(String carta){
        this.agregarCarta(carta, false);
    }
    
    //Retorna la ultima carta que le dieorn al jugador
    public String getLastCarta(boolean separar){
        int s;
        if(separar) s = 1;
        else s = 0;
        
        return this.cartas[this.numCartas[s]-1][s];
    }
    //En caso de no poner el segundo parametro, se llama al metodo anterior con separar = false
    public String getLastCarta(){
        return this.getLastCarta(false);
    }
    
    //Regresa el valor del juego del jugador
    public int getValor(boolean separar){
        int s;
        if(separar) s = 1;
        else s = 0;
        //Revisa si son las cartas originales o las separadas
        int valor, ases;
        valor = ases = 0;
        char c;
        //Toma solo el primer caracter de la carta
        for(int i = 0;i<numCartas[s];i++){
            c = cartas[i][s].charAt(0);
            if(c == 'A')
                ases++;
            else if(c == 'K' || c == 'Q' || c == 'J')
                valor+=10;
            else
                valor+=Integer.parseInt(""+c);
        }
        //Si hay ases y se pasa de 21, los ases valdran 1
        valor += (ases*11);
        while(ases != 0 && valor > 21){
            valor-=10;
            ases--;            
        }
        return valor;
    }
    //En caso de no poner el segundo parametro, se llama al metodo anterior con separar = false
    public int getValor(){
        return this.getValor(false);
    }
    
    //Remueve todas las cartas del jugador
    public void regresarCartas(){
        this.cartas[0] = null;
        this.numCartas[0] = 0;
        this.cartas[1] = null;
        this.numCartas[1] = 0;
    }
}

/*class testing{
    public static void main(String[] args){
        Jugador j = new Jugador(false);
        j.agregarCarta("A♥");
        j.agregarCarta("K♥");
        j.agregarCarta("2♥");
        j.agregarCarta("2♥",true);
        System.out.println(j.getValor(false));
        System.out.println(j.getValor(true));
    }
}*/
