package acciones;

import main.*;
import java.awt.event.*;
/**
 * Acciones del boton quedarse: se acaba el turno del jugador actual
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class Quedarse implements ActionListener {
    public void actionPerformed(ActionEvent e){
        int j = Integer.parseInt(e.getActionCommand());
        //Si j es mayor a 7, entonces se esta jugando el juego separado
        boolean sep = false;
        if(j >= 7){
            sep = true;
            j -=7;
        }
        GUI.pedir.setEnabled(true);
        //Si el jugador separo, y es el juego original, se pasa al juego separado
        if(GUI.jugadores[j].separar && sep == false){
            GUI.pasarTurno(j+7);
        //Si es ultimmo jugador, juega el dealer y se revisa el juego
        }else if(j == GUI.numJugadores||(j-7) == GUI.numJugadores){
            GUI.jugarDealer();
            GUI.checarJuego();
            GUI.jugar.setText("Nueva ronda");
            GUI.jugar.setEnabled(true);
            GUI.pedir.setEnabled(false);
            GUI.quedarse.setEnabled(false);
            GUI.jugar.setActionCommand("ronda");
        //Sino, pasa al siguiente
        }else{
            GUI.pasarTurno(j+1);
        }
    }   
}