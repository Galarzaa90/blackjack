package acciones;

import main.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Acciones del boton empezar juego.
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class empezarJuego implements ActionListener{
    public void actionPerformed(ActionEvent e) {
        GUI.dealer.numCartas[0] = 0;
        GUI.dealInfo.setText("");
        //Si es una ronda, va a preguntar apuesta
        if(e.getActionCommand().equals("ronda")){
            GUI.resetTablero();
            boolean repetir;
            for(int i = 0;i <= GUI.numJugadores; i++){
                do{
                    String str = JOptionPane.showInputDialog(GUI.jugadores[i].nombre+"\nApuesta (Minimo $10):","10");
                    repetir = false;
                    try{
                        GUI.jugadores[i].apuesta[0] = Integer.parseInt(str);
                    } catch(NumberFormatException eof) {
                        JOptionPane.showMessageDialog(null, "Debe ser un valor numerico", "Error", JOptionPane.WARNING_MESSAGE);
                        repetir = true;
                        continue;
                    }
                    if(GUI.jugadores[i].dinero<GUI.jugadores[i].apuesta[0])
                        JOptionPane.showMessageDialog(null, "No puedes apostar mas de lo que tienes!", "Error", JOptionPane.WARNING_MESSAGE);
                    if(GUI.jugadores[i].apuesta[0] < 10)
                        JOptionPane.showMessageDialog(null, "La apuesta minima es $10.", "Error", JOptionPane.WARNING_MESSAGE);
                }while(GUI.jugadores[i].dinero<GUI.jugadores[i].apuesta[0]||GUI.jugadores[i].apuesta[0] < 10||repetir);
                GUI.jugadores[i].dinero -= GUI.jugadores[i].apuesta[0];
            }
        }
        //Crea la baraja
        GUI.Baraja = new Cartas();
        //Le da las cartas al dealer y se las muestra
        GUI.dealer.agregarCarta(GUI.Baraja.tomarCarta());
        GUI.mostrarCarta(GUI.DEALER, 0, null, true);
        GUI.dealer.agregarCarta(GUI.Baraja.tomarCarta());
        GUI.mostrarCarta(GUI.DEALER, 1, GUI.dealer.getLastCarta(), false);
        //Le da las cartas a los jugadores
        for(int i = 0;i <= GUI.numJugadores;i++){
            GUI.jugadores[i].agregarCarta(GUI.Baraja.tomarCarta());
            GUI.mostrarCarta(i, 0, GUI.jugadores[i].getLastCarta(), false);
            GUI.jugadores[i].agregarCarta(GUI.Baraja.tomarCarta());
            GUI.mostrarCarta(i, 1, GUI.jugadores[i].getLastCarta(), false);
            GUI.updateLabel(i);
        }
        //Pone turno al primer jugador y activa los botones
        GUI.setTurno(0);
        GUI.pedir.setEnabled(true);
        GUI.pedir.setActionCommand("0");
        GUI.quedarse.setEnabled(true);
        GUI.quedarse.setActionCommand("0");
        GUI.rendir.setEnabled(true);
        GUI.rendir.setActionCommand("0");
        GUI.doblar.setEnabled(true);
        GUI.doblar.setActionCommand("0");
        //Si el dealer tiene As, activa el boton asegurar
        if(GUI.dealer.getLastCarta().charAt(0) == 'A'){
            GUI.asegurar.setEnabled(true);
            GUI.asegurar.setActionCommand("0");
        }
        //Si las dos cartas del jugador son iguales, se activa separar
        if(GUI.jugadores[0].cartas[0][0].charAt(0) == GUI.jugadores[0].cartas[1][0].charAt(0))
            GUI.separar.setEnabled(true);
        else
            GUI.separar.setEnabled(false);
        GUI.separar.setActionCommand("0");
        GUI.updateDatos();
        GUI.jugar.setEnabled(false);
        GUI.nuevoJ.setEnabled(false);
    }
    
}
