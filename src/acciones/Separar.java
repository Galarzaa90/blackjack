package acciones;

import java.awt.*;
import main.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class Separar implements ActionListener{
    public void actionPerformed(ActionEvent e) {
        int j = Integer.parseInt(e.getActionCommand());
        if(GUI.jugadores[j].dinero >= 10){
            boolean repetir;
            do{
                String str = JOptionPane.showInputDialog(GUI.jugadores[j].nombre+"\nApuesta (Minimo $10):","10");
                repetir = false;
                try{
                    GUI.jugadores[j].apuesta[1] = Integer.parseInt(str);
                } catch(NumberFormatException eof) {
                    JOptionPane.showMessageDialog(null, "Debe ser un valor numerico", "Error", JOptionPane.WARNING_MESSAGE);
                    repetir = true;
                    continue;
                }
                if(GUI.jugadores[j].dinero<GUI.jugadores[j].apuesta[1])
                    JOptionPane.showMessageDialog(null, "No puedes apostar mas de lo que tienes!", "Error", JOptionPane.WARNING_MESSAGE);
                if(GUI.jugadores[j].apuesta[1] < 10)
                JOptionPane.showMessageDialog(null, "La apuesta minima es $10.", "Error", JOptionPane.WARNING_MESSAGE);
            }while(GUI.jugadores[j].dinero<GUI.jugadores[j].apuesta[1]||repetir);
            GUI.jugadores[j].dinero -= GUI.jugadores[j].apuesta[1];
            GUI.jugadores[j].separar = true;
            GUI.frame.setSize(new Dimension(550, 600));
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            GUI.frame.setLocation((dim.width-GUI.frame.getSize().width)/2, (dim.height-GUI.frame.getSize().height)/2);
            for(int i = 0;i<5;i++){
                GUI.cartJug[j][i][1].setVisible(true);
            }
            GUI.jugadores[j].agregarCarta(GUI.jugadores[j].getLastCarta(), true);
            GUI.mostrarCartaSeparar(j, 0, GUI.jugadores[j].getLastCarta(true));
            GUI.jugadores[j].agregarCarta(GUI.Baraja.tomarCarta(), true);
            GUI.mostrarCartaSeparar(j, 1, GUI.jugadores[j].getLastCarta(true));
            GUI.jugadores[j].numCartas[0]--;
            GUI.borrarCarta(j,1);
            GUI.jugadores[j].agregarCarta(GUI.Baraja.tomarCarta());
            GUI.mostrarCarta(j, 1, GUI.jugadores[j].getLastCarta(), false);
            GUI.updateDatos();
            GUI.updateLabel(j);
            GUI.updateLabelSep(j);
            GUI.asegurar.setEnabled(false);
            GUI.rendir.setEnabled(false);
        }else{
            JOptionPane.showMessageDialog(null, "No tienes suficiente dinero", "Error", JOptionPane.WARNING_MESSAGE);
        }
        GUI.separar.setEnabled(false);

    }

}