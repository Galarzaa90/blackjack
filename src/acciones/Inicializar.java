package acciones;

import main.*;
import java.awt.event.*;
import javax.swing.JOptionPane;

/**
 * Acciones del boton Iniciar: que da los valores iniciales al juego
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class Inicializar implements ActionListener{
    public void actionPerformed(ActionEvent e) {
        GUI.resetTablero();
        //Borra a todos los jugadores y al dealer
        for(int j = 0;j<7;j++){
            GUI.jugadores[j] = null;
        }
        GUI.dealer = null;
        //Pide el costo de entrada del juego
        do{
            String str = JOptionPane.showInputDialog("Costo de entrada(Minimo $100):","100");
            try{
                GUI.entrada = Integer.parseInt(str);
            } catch(NumberFormatException f) {
                JOptionPane.showMessageDialog(null, "Debe ser un valor numerico", "Error", JOptionPane.WARNING_MESSAGE);
                continue;
            }
            if(GUI.entrada < 100)
                JOptionPane.showMessageDialog(null, "La entrada minima es $100.", "Error", JOptionPane.WARNING_MESSAGE);
        }while(GUI.entrada<100);
        //Desactiva botones y resetea contadores;
        GUI.numJugadores = 0;
        GUI.reset.setText("Reiniciar");
        GUI.reset.setActionCommand("reiniciar");
        GUI.pedir.setEnabled(false);
        GUI.quedarse.setEnabled(false);
        GUI.separar.setEnabled(false);
        GUI.doblar.setEnabled(false);
        GUI.asegurar.setEnabled(false);
        GUI.rendir.setEnabled(false);     
        //Borra etiquetas de informacion
        for(int i = 0; i < 7;i++){
            GUI.info[i][0].setText("");
            GUI.info[i][1].setText("");
        }
        //Activa botones de jugar y nuevo jugador
        GUI.jugar.setEnabled(true);
        GUI.jugar.setText("Comenzar juego");
        GUI.jugar.setActionCommand("new");
        GUI.nuevoJ.setEnabled(true);
        //Crea al jugador 1 y al dealer
        GUI.dealer = new Jugador(false);
        GUI.jugadores[0] = new Jugador();
        GUI.updateLabel(0);
        GUI.dealInfo.setText("");
        GUI.updateDatos();
    }
    
}