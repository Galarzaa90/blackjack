package acciones;

import main.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Acciones del boton doblar
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class Doblar implements ActionListener{
    public void actionPerformed(ActionEvent e) {
        int j = Integer.parseInt(e.getActionCommand());
        //Si j es mayor a 7, entonces se esta jugando el juego separado
        boolean sep = false;
        if(j >= 7){
            sep = true;
            j -=7;
        }
        //Revisa si hay dinero para doblar la apuesta
        if(((GUI.jugadores[j].dinero < GUI.jugadores[j].apuesta[0])&&!sep)||((GUI.jugadores[j].dinero < GUI.jugadores[j].apuesta[1])&&sep)){
            if(sep)
                JOptionPane.showMessageDialog(null, "Dinero insuficiente.\nOcupas tener $"+GUI.jugadores[j].apuesta[1]+" o mas.", GUI.jugadores[j].nombre, JOptionPane.WARNING_MESSAGE);
            else
                JOptionPane.showMessageDialog(null, "Dinero insuficiente.\nOcupas tener $"+GUI.jugadores[j].apuesta[0]+" o mas.", GUI.jugadores[j].nombre, JOptionPane.WARNING_MESSAGE);
            GUI.doblar.setEnabled(false);
        }else{
            //Si es el juego separado, agrega la carta y la apuesta al juego separado
            if(sep){
                GUI.jugadores[j].dinero-=GUI.jugadores[j].apuesta[1];
                GUI.jugadores[j].apuesta[1]*=2;
                GUI.jugadores[j].agregarCarta(GUI.Baraja.tomarCarta(),true);
                GUI.mostrarCartaSeparar(j, GUI.jugadores[j].numCartas[1]-1, GUI.jugadores[j].getLastCarta(true));
                GUI.updateLabelSep(j);
            //Sino, las agrega al juego original
            }else{
                GUI.jugadores[j].dinero-=GUI.jugadores[j].apuesta[0];
                GUI.jugadores[j].apuesta[0]*=2;
                GUI.jugadores[j].agregarCarta(GUI.Baraja.tomarCarta());
                GUI.mostrarCarta(j, GUI.jugadores[j].numCartas[0]-1, GUI.jugadores[j].getLastCarta(), false);
                GUI.updateLabel(j); 
            }
            GUI.updateDatos();
            //Si el jugador separo y no es el juego separado, pasa al separado
            if(GUI.jugadores[j].separar && sep == false){
                GUI.pasarTurno(j+7);
            //Si es el ultimo jugador, juega el dealer y se checa el juego
            }else if(j == GUI.numJugadores){
                GUI.jugarDealer();
                GUI.checarJuego();
            //Sino, solo se pasa turno
            }else{
                GUI.pasarTurno(j+1);
            }
        }

    }

}