package acciones;

import main.*;
import java.awt.event.*;

/**
 * Acciones del boton nuevo jugador: Agrega mas jugadores a la partida
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class nuevoJugador implements ActionListener{
    public void actionPerformed(ActionEvent e) {
        GUI.numJugadores++;
        GUI.jugadores[GUI.numJugadores] = new Jugador();
        GUI.updateDatos();
        GUI.updateLabel(GUI.numJugadores);
        if(GUI.numJugadores >= 6) //Si se alcanza el numero de jugadores maximo, se desactiva
            GUI.nuevoJ.setEnabled(false);
    }
    
}