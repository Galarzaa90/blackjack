package acciones;

import main.*;
import java.awt.event.*;
import javax.swing.*;
/**
 * Acciones para el boton dar carta
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class darCarta implements ActionListener{
    public void actionPerformed(ActionEvent e) {
        int j = Integer.parseInt(e.getActionCommand());
        //Si j es mayor a 7, entonces se esta jugando el juego separado
        boolean sep = false;
        if(j >= 7){
            sep = true;
            j -=7;
        }
        //Rendir, doblar y asegurar solo se permiten en la primera movida, asi que se desatctivan
        GUI.rendir.setEnabled(false);
        GUI.doblar.setEnabled(false);
        GUI.asegurar.setEnabled(false);
        //Si es el juego separado, las agrega al tablero separado
        if(sep){
            GUI.jugadores[j].agregarCarta(GUI.Baraja.tomarCarta(),true);
            GUI.mostrarCartaSeparar(j, GUI.jugadores[j].numCartas[1]-1, GUI.jugadores[j].getLastCarta(true));
            GUI.updateLabel(j);
            GUI.updateLabelSep(j);
        //Sino, las agrega en el tablero principal
        }else{
            GUI.jugadores[j].agregarCarta(GUI.Baraja.tomarCarta());
            GUI.mostrarCarta(j, GUI.jugadores[j].numCartas[0]-1, GUI.jugadores[j].getLastCarta(), false);
            GUI.updateLabel(j);
        }
        //Si se llega al numero maximo de cartas, se desactiva el boton de pedir cartas
        if((GUI.jugadores[j].numCartas[0] == 5&& !sep)||(GUI.jugadores[j].numCartas[1] == 5&&sep))
            GUI.pedir.setEnabled(false);
        //Si se pasa de 21, se acaba el turno
        if((GUI.jugadores[j].getValor() > 21 && !sep)||(GUI.jugadores[j].getValor(true) > 21 && sep)){
            //Si es un solo jugador
            if(GUI.numJugadores == 0){
                GUI.resultado(j, false, 1);
                GUI.mostrarCarta(GUI.DEALER, 0, GUI.dealer.cartas[0][0], false);
                GUI.pedir.setEnabled(false);
                GUI.quedarse.setEnabled(false);
                GUI.jugar.setText("Nueva ronda");
                GUI.jugar.setEnabled(true);
                GUI.jugar.setActionCommand("ronda");
            }else{
                JOptionPane.showMessageDialog(null, "Pasaste 21, se acabo tu tuno.", GUI.jugadores[j].nombre, JOptionPane.WARNING_MESSAGE);
                //Si el jugador separo y no es el juego separado, pasa al separado
                if(GUI.jugadores[j].separar && sep == false){
                    GUI.pasarTurno(j+7);
                //Si es el ultimo jugador, juega el dealer y se checa el juego
                }else if(j == GUI.numJugadores){
                    GUI.jugarDealer();
                    GUI.checarJuego();
                //Sino, solo se pasa turno
                }else{
                    GUI.pasarTurno(j+1);
                }
            }
            
        }
    }
    
}