package acciones;

import main.*;
import java.awt.event.*;

/**
 * Acciones del boton rendirse
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class Rendirse implements ActionListener{
    public void actionPerformed(ActionEvent e) {
        int j = Integer.parseInt(e.getActionCommand());
        GUI.jugadores[j].rendir = true;
        //Si es el ultimo jugador, juega el dealer y se checa el juego
        if(j == GUI.numJugadores){
            GUI.jugarDealer();
            GUI.pedir.setEnabled(false);
            GUI.quedarse.setEnabled(false);
            GUI.rendir.setEnabled(false);
            GUI.checarJuego();
        //Sino, pasa turno
        }else{
            GUI.pasarTurno(j+1);
        }
    }

}