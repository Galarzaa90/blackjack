package acciones;

import main.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Acciones del boton asegurar
 * @author Allan Galarza Susarrey, Jose Meza Ibarra, Adrian Llanes Borquez
 */
public class Asegurar implements ActionListener{
    public void actionPerformed(ActionEvent e) {
        int j = Integer.parseInt(e.getActionCommand());
        //Revisa si hay dinero para una aseguranza
        if(GUI.jugadores[j].dinero < (GUI.jugadores[j].apuesta[0]/2)){
            JOptionPane.showMessageDialog(null, "Dinero insuficiente.\nOcupas tener $"+GUI.jugadores[j].apuesta[0]/2+" o mas.", GUI.jugadores[j].nombre, JOptionPane.WARNING_MESSAGE);
        }else{
            //Quita el dinero que se puso para la aseguranza
            GUI.jugadores[j].asegurar = true;
            GUI.jugadores[j].dinero -= GUI.jugadores[j].apuesta[0]/2;
            GUI.updateLabel(j);
            GUI.updateDatos();
        }
        GUI.asegurar.setEnabled(false);
    }

}